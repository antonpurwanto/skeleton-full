
ALTER DATABASE `gendb`
DEFAULT CHARSET utf8
DEFAULT COLLATE utf8_general_ci ;

create table product (
  id VARCHAR(36),
  code VARCHAR (20) NOT NULL ,
  name VARCHAR (50) NOT NULL ,
  price DECIMAL (10,2) NULL,
  PRIMARY KEY (id),
  UNIQUE (code)
);

insert into product (id, code, name) VALUES
('p001', 'P-001', 'Product 001'),
('p002', 'P-002', 'Product 002'),
('p003', 'P-003', 'Product 003'),
('p004', 'P-004', 'Product 004'),
('p005', 'P-005', 'Product 005');


